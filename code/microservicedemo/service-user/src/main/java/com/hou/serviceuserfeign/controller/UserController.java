package com.hou.serviceuserfeign.controller;

import com.hou.serviceuserfeign.feign.OrderFeignClient;
import com.hou.serviceuserfeign.feign.OrderFeignClientByUrl;
import com.hou.serviceuserfeign.po.Order;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/feign")
public class UserController {

    //注入Feign接口,直接调用
    @Autowired private OrderFeignClient orderFeignClient;
    @Autowired private OrderFeignClientByUrl orderFeignClientByUrl;

    @GetMapping("/ordername")
    public String getOrderName(){
        return orderFeignClient.getOrderName();
    }

    @GetMapping("/order")
    @HystrixCommand(fallbackMethod="getFallback") // 如果当前调用的get()方法出现了错误，则执行fallback
    public Order getOrder(String id){
        System.out.println(id);
        return orderFeignClientByUrl.getOrder(id);
    }

    //服务失败回调方法,必须与原来方法参数保持一致
    public Order getFallback(String id){
        return new Order("-1","服务挂了");
    }

}
