package com.hou.serviceuserfeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients  //启动Feign
@EnableCircuitBreaker  //启用熔断
public class ServiceUserFeignApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceUserFeignApplication.class, args);
    }
}
