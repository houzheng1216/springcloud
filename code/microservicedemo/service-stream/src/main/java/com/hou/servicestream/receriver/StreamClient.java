package com.hou.servicestream.receriver;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @Input和@Output定义输出通道和输入通道
 */
public interface StreamClient {
    //定义通道名称
    String INPUT = "myinput";
    String OUTPUT = "myoutput";

    //返回通道对象用一个发送或者接受消息
    @Input(StreamClient.INPUT)
    SubscribableChannel input();

    @Output(StreamClient.OUTPUT)
    MessageChannel output();

}
