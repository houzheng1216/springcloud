package com.hou.servicestream.receriver;

import com.hou.servicestream.User;
import org.springframework.amqp.core.Message;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * @EnableBinding 注解用来指定一个或多个定义了 @Input 或 @Output 注解的接口
 * 以此实现对消息通道（Channel）的绑定
 */
@Component
@EnableBinding(value = StreamClient.class)
public class StreamReceiver {

    //接受消息
    @StreamListener(StreamClient.INPUT)
    public void receive(User user) {
        System.out.println("接受到消息:"+user.toString());
    }

    //接受消息
    @StreamListener(StreamClient.OUTPUT)
    @SendTo(StreamClient.INPUT)
    public User receive1(User user) {
        System.out.println("out接受到消息:"+user.toString());
        return user;
    }
}
