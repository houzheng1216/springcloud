package com.hou.servicestream.sender;

import com.hou.servicestream.User;
import com.hou.servicestream.receriver.StreamClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SenderController {

    //注入定义的接口,使用通道发送方法
    @Autowired
    private StreamClient streamClient;

    @RequestMapping("/send")
    public String sendString(Integer id){
        User user = new User();
        user.setId(id);
        streamClient.output().send(MessageBuilder.withPayload(user).setHeader("msgId",id).build());
        return "success";
    }
}
