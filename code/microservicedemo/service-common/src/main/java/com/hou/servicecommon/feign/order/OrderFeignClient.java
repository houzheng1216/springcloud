package com.hou.servicecommon.feign.order;

import com.hou.servicecommon.feign.order.impl.OrderFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 用于通知Feign组件对该接口进行代理，name属性指定我们要调用哪个服务。
 * 使用者可直接通过@Autowired注入。
 */

/**
 * spring Cloud允许通过注解@FeignClient的configuration属性自定义Feign的配置，
 * 自定义配置的优先级比FeignClientsConfiguration要高
 */
@Component
@FeignClient(value = "service-order",path = "/order",fallback = OrderFeignClientFallback.class)
public interface OrderFeignClient {

    /**
     * Spring Cloud应用在启动时，Feign会扫描标有@FeignClient注解的接口，生成代理，并注册到Spring容器中
     * 生成代理时Feign会为每个接口方法创建一个RequetTemplate对象，该对象封装了HTTP请求需要的全部信息，
     * 请求参数名、请求方法等信息都是在这个过程中确定的，Feign的模板化就体现在这里
     * @return
     */
    @GetMapping("/order/name")
    String getOrderName();
}
