package com.hou.servicecommon.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 限流注解
 */
@Target(ElementType.METHOD)  //作用与方法上
@Retention(RetentionPolicy.RUNTIME) //注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在
@Documented
public @interface ApiRateLimit {

    int value(); //控制并发最大数量
}
