package com.hou.servicecommon.feign.order.impl;

import com.hou.servicecommon.feign.order.OrderFeignClient;
import org.springframework.stereotype.Component;

/**
 * Feign中使用Fallback需要使用feing接口的实现类
 */
@Component
public class OrderFeignClientFallback implements OrderFeignClient {

    @Override
    public String getOrderName() {
        return "服务无效";
    }
}
