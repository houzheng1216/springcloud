package com.hou.serviceorder.controller;

import com.hou.servicecommon.entity.Order;
import com.hou.servicecommon.annotation.ApiRateLimit;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author houzheng
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @ApiRateLimit(value = 5)
    @GetMapping("/name")
    public String getOrderName(){
        return "order";
    }

    @PostMapping("/insert")
    public Order insertOrder(@RequestBody Order order){
        return order;
    }
}
