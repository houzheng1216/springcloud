package com.hou.elasticjob.listener;

import com.dangdang.ddframe.job.executor.ShardingContexts;
import com.dangdang.ddframe.job.lite.api.listener.ElasticJobListener;

import java.time.LocalDate;

public class MyElasticJobListener implements ElasticJobListener {
    @Override
    public void beforeJobExecuted(ShardingContexts shardingContexts) {
        System.out.println("JOB BEGIN TIME:"+LocalDate.now().toString());
    }

    @Override
    public void afterJobExecuted(ShardingContexts shardingContexts) {
        System.out.println("JOB END TIME:"+LocalDate.now().toString());
    }
}