package com.hou.elasticjob.config;

import com.dangdang.ddframe.job.event.rdb.JobEventRdbConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.spring.api.SpringJobScheduler;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.hou.elasticjob.utils.ElasticJobUtils;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * 配置脚本Job
 */
@Configuration
public class MyScriptJobConf {
    @Autowired ZookeeperRegistryCenter regCenter;
    @Autowired
    DataSource hikariDataSource;
    @Autowired
    ApplicationContext applicationContext;
    /**
     * 配置任务调度: 参数:  任务
     *                    zk注册中心
     *                    任务详情
     */
    @Bean(initMethod = "init")
    public JobScheduler scriptJobScheduler(@Value("${myScriptJob.cron}") final String cron,  //yml注入
                                           @Value("${myScriptJob.shardingTotalCount}") final int shardingTotalCount,
                                           @Value("${myScriptJob.shardingItemParameters}") final String shardingItemParameters) {
        return new SpringJobScheduler(null, regCenter,
                                      ElasticJobUtils.getScriptJobConfiguration(
                                              "script_job",
                                              cron,
                                              shardingTotalCount,
                                              //命令或者脚本路径
                                              shardingItemParameters,"echo hello")
                //添加事件追踪,可将记录持久化到数据库
                ,new JobEventRdbConfiguration(hikariDataSource)
                                              //,new MyElasticJobListener() 可配置监听器
        );
    }
}
